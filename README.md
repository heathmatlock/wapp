wapp
---

## develop
```
make database
make client
make server
make run
```

## build & run a release 
```
nix-build nix/release.nix
cd result
./bin/server
```
