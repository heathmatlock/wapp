let pkgs = import ./nix/nixpkgs.nix ;
in {
  server = (pkgs.haskell.packages.ghc.callCabal2nix "wapp" ./. {}).env;
  client = (pkgs.haskell.packages.ghcjs.callCabal2nix "wapp" ./. {}).env;
}

