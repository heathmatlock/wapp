{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

import qualified Common                        as C
import qualified Miso
import           Miso                           ( App(..) )
import qualified Miso.String                   as Miso
import           Servant.API                    ( (:<|>)(..) )
import           Servant.Client.Ghcjs
import           Network.HTTP.Client            ( newManager
                                                , defaultManagerSettings
                                                )


position :<|> hello :<|> marketing = client C.clientEndpoints

main :: IO ()
main = Miso.miso $ \currentURI -> App { initialAction = C.NoOp
                                      , model = C.initialModel currentURI
                                      , update        = updateModel
                                      , view          = C.viewModel
                                      , events        = Miso.defaultEvents
                                      , subs = [Miso.uriSub C.HandleURIChange]
                                      , mountPoint    = Nothing
                                      }

updateModel :: C.Action -> C.Model -> Miso.Effect C.Action C.Model updateModel action m = case action of C.NoOp -> m Miso.<# do
    pure C.NoOp
  C.AddOne -> m Miso.<# do
    pure C.NoOp
  C.SubtractOne -> m Miso.<# do
    pure C.NoOp
  C.ChangeURI uri -> m Miso.<# do
    Miso.pushURI uri
    pure C.NoOp
  C.HandleURIChange uri -> m { C._uri = uri } Miso.<# do
    pure C.NoOp
  C.RegisterUser -> m Miso.<# do
    pos <- foop
    case pos of
      Left  err -> Miso.alert (Miso.toMisoString $ show err)
      Right pos -> Miso.alert (Miso.toMisoString $ show pos)
    pure C.NoOp

foop :: IO (Either ClientError C.Position)
foop = do
  ePos <- runClientM $ position 10 20
  pure ePos
