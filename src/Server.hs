{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

import           Common                         ( Action
                                                , ClientAPI
                                                , HelloMessage(..)
                                                , ViewRoutes
                                                , Position(..)
                                                , ClientInfo
                                                , Email
                                                , emailForClient
                                                , flippedLink
                                                , initialModel
                                                , homeLink
                                                , viewModel
                                                , page404View
                                                )
import           Data.Proxy
import qualified Lucid                         as L
import qualified Lucid.Base                    as L
import qualified Miso
import           Miso                           ( View )
import qualified Network.HTTP.Types            as HTTP
import qualified Network.Wai                   as Wai
import qualified Network.Wai.Handler.Warp      as Wai
import qualified Network.Wai.Middleware.Gzip   as Wai
import qualified Network.Wai.Middleware.RequestLogger
                                               as Wai
import qualified Servant
import           Servant                        ( (:<|>)(..)
                                                , (:>)
                                                )
import qualified System.IO                     as IO

main :: IO ()
main = do
  IO.hPutStrLn IO.stderr "Running on port 3003..."
  Wai.run 3003 $ Wai.logStdout $ compress app
 where
  compress :: Wai.Middleware
  compress = Wai.gzip Wai.def { Wai.gzipFiles = Wai.GzipCompress }

app :: Wai.Application
app = Servant.serve
  (Proxy @ServerAPI)
  (static :<|> clientHandlers :<|> serverHandlers :<|> Servant.Tagged page404)
 where
  static :: Servant.Server StaticAPI
  static = Servant.serveDirectoryFileServer "static"

  clientHandlers :: Servant.Server ClientAPI
  clientHandlers = position :<|> hello :<|> marketing

  position :: Int -> Int -> Servant.Handler Position
  position x y = return (Position x y)

  hello :: Maybe String -> Servant.Handler HelloMessage
  hello mname = return . HelloMessage $ case mname of
    Nothing -> "Hello, anonymous coward"
    Just n  -> "Hello, " ++ n

  marketing :: ClientInfo -> Servant.Handler Email
  marketing clientinfo = return (emailForClient clientinfo)

  serverHandlers :: Servant.Server ServerRoutes
  serverHandlers = homeServer :<|> flippedServer
  -- Alternative type:
  -- Servant.Server (ToServerRoutes C.Home HtmlPage C.Action)
  -- Handles the route for the home page, rendering C.homeView.
  homeServer :: Servant.Handler (HtmlPage (View Action))
  homeServer = pure $ HtmlPage $ viewModel $ initialModel homeLink
  -- Alternative type:
  -- Servant.Server (ToServerRoutes C.Flipped HtmlPage C.Action)
  -- Renders the /flipped page.
  flippedServer :: Servant.Handler (HtmlPage (View Action))
  flippedServer = pure $ HtmlPage $ viewModel $ initialModel flippedLink
  -- The 404 page is a Wai application because the endpoint is Raw.
  -- It just renders the page404View and sends it to the client.
  page404 :: Wai.Application
  page404 _ respond =
    respond
      $ Wai.responseLBS HTTP.status404 [("Content-Type", "text/html")]
      $ L.renderBS
      $ L.toHtml page404View

-- | Represents the top level Html code. Its value represents the body of the
-- page.
newtype HtmlPage a = HtmlPage a
  deriving (Show, Eq)

instance L.ToHtml a => L.ToHtml (HtmlPage a) where

  toHtmlRaw = L.toHtml

  toHtml (HtmlPage x) = do
    L.doctype_
    L.head_ $ do
      L.title_ "Miso isomorphic example"
      L.meta_ [L.charset_ "utf-8"]
      L.with
        (L.script_ mempty)
        [ L.makeAttribute "src" "/static/all.js"
        , L.makeAttribute "async" mempty
        , L.makeAttribute "defer" mempty
        ]
    L.body_ (L.toHtml x)

-- Converts the ClientRoutes (which are a servant tree of routes leading to
-- some `View action`) to lead to `Get '[Html] (HtmlPage (View C.Action))`
type ServerRoutes = Miso.ToServerRoutes ViewRoutes HtmlPage Action

-- The server serves static files besides the ServerRoutes, among which is the
-- javascript file of the client.
type ServerAPI
  = StaticAPI :<|> ( -- This will show the 404 page for any unknown route
                    ClientAPI :<|> ServerRoutes :<|> Servant.Raw)

type StaticAPI = "static" :> Servant.Raw
