let

  pkgs = import ./nixpkgs.nix ;
  wapp-src = ../. ;

  server = pkgs.haskell.packages.ghc.callCabal2nix "wapp" ../. {};
  client = pkgs.haskell.packages.ghcjs.callCabal2nix "wapp" ../. {};

in

  pkgs.runCommand "wapp" { inherit client server; } ''
    mkdir -p $out/{bin,static,data}
    cp ${server}/bin/* $out/bin/
    ${pkgs.closurecompiler}/bin/closure-compiler ${client}/bin/client.jsexe/all.js > $out/static/all.js
    cp ${wapp-src}/static/* $out/static/
  ''

